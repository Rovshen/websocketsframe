package model;

/**
 * This imports are needed for stream manipulation, exceptions, 
 * and data parsing into bytes
 */
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import exception.BadValueException;

/**
 * @(#)WebSocketFrame.java      v1.0 1/29/2014
 * 
 * Copyright (c): This class was designed as a first home work 
 * assignment for Advanced Data communication class by Rovshen Nazarov. 
 * All rights reserved.
 * 
 * @version 1.0 1/29/2014
 * @author Author: Rovshen Nazarov
 * Assignment Title: Web Socket Frame according to RFC 6455
 * Assignment Description: This class allows serialization and 
 *                              de-serialization of the frame object.
 * Due Date: 1/30/2014
 * Date Created: 1/24/2014
 * Date Last Modified: 1/29/2014
 */

/**
 * Data Abstracation: Frame according to RFC 6455 Input: Byte array
 * Implementation: Parsing, encoding, decoding Output: a frame Assumptions: byte
 * array is provided on construction Implementation: The input stream is used to
 * get bytes, parse them and store into the frame object. Also possible to
 * generate a frame object and then generate a byte array from it. Output: None,
 * but writes to a provided output stream. Assumptions: It is assumed that the
 * input stream is supplied to the costructor and on enode output stream is
 * supplied to the encode method. Also it is assumed that the bytes in the input
 * stream form a frame that complies with RFC 6455
 */

public class WebSocketFrame {
    
    /**
     * Defines the frame opcodes that are currently supported by this class.
     */
    public enum Opcode {
        CONTINUATION_FRAME(0x0), // %x0 denotes a continuation frame
        TEXT_FRAME(0x1), // %x1 denotes a text frame
        BINARY_FRAME(0x2), // %x2 denotes a binary frame
        // %x3-7 are reserved for further non-control frames
        CLOSE_CONNECTION(0x8), // %x8 denotes a connection close
        PING(0x9), // %x9 denotes a ping
        PONG(0xA); // %xA denotes a pong
        
        /**
         * The visible label.
         */
        private final int label;
        
        /**
         * Constructs a new {@link Opcode} instance.
         * 
         * @param label
         * the {@link #label}
         */
        private Opcode(int label) {
        
            this.label = label;
        }
        
        /**
         * Returns the {@link #label}.
         * 
         * @return the {@link #label}.
         */
        public int getValue() {
        
            return label;
        }
        
        /**
         * Returns opcode given a numeric code.
         * 
         * @param code
         * - numeric code
         * @return Opcode if valid numeric code; otherwise null
         */
        public static Opcode getOpcode(int setKey) throws BadValueException {
        
            // for continuation bit I have to check equality as potentially the
            // whole byte will be 0s
            // go via all supported opcodes and check if any of them are set
            if (setKey == 0) {
                return Opcode.CONTINUATION_FRAME;
            } else {
                for (Opcode key : Opcode.values()) {
                    if (((setKey == key.getValue()) && (key.getValue() != 0))) { return key; }
                }
                throw new BadValueException("Unsupported opcode");
                
            }
        }
    }// end of Opcode
    
    // class attributes
    protected boolean          fin;
    protected boolean          masked;
    protected Opcode           opcode;
    protected byte[]           payload;
    protected static final int MAX_CONTROL_PAYLOAD_LENGTH = 125;
    
    /**
     * Constructs a frame object with the given attributes.
     * 
     * @param fin
     * true if this frame is the last frame in a message, false
     * otherwise
     * @param opcode
     * the type of frame
     * @param payload
     * the data that will be sent in the body of this frame
     * @throws BadValueException
     * if opcode is null
     * @throws BadValueException
     * if opcode or payload is null
     */
    public WebSocketFrame(boolean fin, Opcode opcode, byte[] payload)
            throws BadValueException {
    
        if (opcode == null) { throw new BadValueException(
                "The opcode cannot be NULL"); }
        if (payload == null) { throw new BadValueException(
                "The payload cannot be NULL"); }
        this.fin = fin;
        this.opcode = opcode;
        this.payload = payload;
        validateFrame();
    }
    
    /**
     * Constructs a frame object by deserializing one frame from the given
     * input stream and sets the internal state of this object to match what
     * is described by the frame.
     * The web-socket frame serialization format is described in RFC 6455.
     * 
     * @param in
     * the stream to read a frame from
     * @param expectMask
     * true if the frame is expected to be masked, false otherwise
     * @throws IOException
     * if a read error occurs (i.e. unexpected EOF)
     * or if 'in' is null
     * @throws BadValueException
     * if an invalid frame is encountered while reading,
     * including if the frame's masking state is unexpected
     */
    public WebSocketFrame(InputStream in, boolean expectMask)
            throws IOException, BadValueException {
    
        byte[] tempHeaderArr = new byte[2];
        int readBytes = 0;
        if (in == null) { throw new IOException("the InputStream in is null"); }
        readBytes = fillByteArray(in, tempHeaderArr);
        if (readBytes != 2) { throw new IOException(
                "short frame" + readBytes); }
        if (readBytes == -1) { throw new IOException(
                "End of stream encountered"); }
        
        fin = (tempHeaderArr[0] & (byte) 0x80) != 0;
        // check RSV bits
        if ((tempHeaderArr[0] & (byte) 0x70) != 0) { // check if RSV
            throw new BadValueException("RSV is not 0");
        }
        
        int opcodeKey = tempHeaderArr[0] & (byte) 0xF;
        opcode = Opcode.getOpcode(opcodeKey);
        
        // is masked
        masked = (tempHeaderArr[1] & (byte) 0x80) != 0;
        if (!expectMask && masked) { throw new BadValueException(
                "Mask is set but was not expected to be set"); }
        
        int secondByteLength = tempHeaderArr[1] & (byte) 0x7F;
        if (secondByteLength < 0) {
            secondByteLength += 128;
        }
        
        long payloadLength = 0L;
        byte[] tempLength;
        
        if (secondByteLength < 126) {
            payloadLength = secondByteLength;
        } else if (secondByteLength == 126) {
            /* get 2 bytes */
            tempLength = new byte[2];
            readBytes = fillByteArray(in, tempLength);
            
            if (readBytes != -1 && readBytes == 2) {
                payloadLength = util.Util.decodeByteArrToLong(tempLength,
                        0, Short.SIZE / Byte.SIZE);
                if (payloadLength < 126) { throw new BadValueException(
                        "incorrect 2 bytes payload length"); }
            } else {
                throw new BadValueException(
                        "Payload length incorrectly encoded.");
            }
        } else if (secondByteLength == 127) {
            /* get 8 bytes */
            tempLength = new byte[8];
            readBytes = fillByteArray(in, tempLength);
            
            /** MUST be 0 most significant bit */
            if (readBytes != -1 && readBytes == 8
                    && (tempLength[0] & 0x80) == 0) {
                payloadLength = util.Util.decodeByteArrToLong(tempLength,
                        0, Long.SIZE / Byte.SIZE);
                if (payloadLength <= 65535) { throw new BadValueException(
                        "incorrect 8 bytes payload length"); }
            } else {
                throw new BadValueException(
                        "Payload length incorrectly encoded.");
            }
        }
        /** get the masking key if the masking key set to true */
        byte[] maskingKey = new byte[4];
        
        if (masked) {
            /* read in 4 bytes */
            readBytes = fillByteArray(in, maskingKey);
            if (readBytes != 4 || readBytes == -1) { throw new IOException(
                    "the masking key MUST be 4 bytes."); }
        }
        byte[] payload = new byte[0];
        /* check that the payload length */
        if (payloadLength <= Integer.MAX_VALUE) {
            if (payloadLength > 0) {
                payload = new byte[(int) payloadLength];
                readBytes = fillByteArray(in, payload);
                if (readBytes == -1 || readBytes != payloadLength) { throw new IOException(
                        "Failed to load payload" + " expected: "
                                + payloadLength + ", but was " + readBytes); }
            }
            /** payload length too large (> 2^32-1) */
        } else {
            throw new BadValueException(
                    "The payload MUST be less than or equal to Integer.MAX_VALUE");
        }
        
        if (!masked) {
            this.payload = payload;
        }
        
        if (masked) {
            this.payload = util.Util.applyMaskingAlgorithm(maskingKey, payload);
        }
        validateFrame();
    }// end of receiving frame data
    
    /**
     * Constructs a frame object by interpreting a previously de-framed byte
     * array.
     * 
     * @param frame
     * the previously de-framed byte array
     * @param expectMask
     * true if the frame is expected to be masked, false otherwise
     * @throws BadValueException
     * if the bytes represent an invalid frame, including
     * if the length of the byte array is incorrect, and
     * including if the frame's masking state is unexpected
     * @throws IOException
     */
    public WebSocketFrame(byte[] frame, boolean expectMask)
            throws BadValueException, IOException
    {
    
        this(new BufferedInputStream(new ByteArrayInputStream(
                frame)), expectMask);
    }
    
    /**
     * Serializes the state of this object into a web-socket frame (see RFC 6455
     * for the specification of the serialized frame) and writes that frame to
     * an output stream. This version of encode() does NOT set a masking key in
     * the encoded frame. The frame cannot be encoded if the state of this
     * object violates the web socket frame protocol specification (RFC 6455).
     * Currently, there are two ways that the specification can be violated: 1.
     * This frame is a CLOSE frame with a non empty payload (we do not currently
     * support close frame status codes) 2. This frame is a control frame with a
     * payload length greater than 125 bytes. WITHOUT masking
     * 
     * @param out
     * the stream to write to
     * @throws IOException
     * if a write error occurs or if 'out' is null
     * @throws BadValueException
     * if the encode cannot take place because the state of this
     * frame violates the protocol specification (see method
     * description for details)
     */
    public void encode(OutputStream out) throws IOException {
    
        /* if out is null throw IOException */
        if (out == null) { throw new IOException("The output stream is null."); }
        /* get the header */
        masked = false;
        out.write(factoredEncode());
        out.write(getPayload());
        out.flush();
        
    }// end of simple encode
    
    /**
     * Serializes the state of this object into a web-socket frame (see RFC 6455
     * for the specification of the serialized frame) and writes that frame to
     * an output stream. This version of encode() DOES set a masking key in the
     * encoded frame.
     * 
     * @param out
     * the stream to write to
     * @param maskingKey
     * the 4 byte masking key to use for encoding
     * @throws IOException
     * if a write error occurs
     * @throws BadValueException
     * if the masking key given is invalid
     */
    public void encode(OutputStream out, byte[] maskingKey) throws IOException,
            BadValueException {
    
        /* if out is null throw IOException */
        if (out == null) { throw new IOException("The output stream is null."); }
        if (maskingKey == null) { throw new BadValueException(
                "The masking key is null."); }
        
        if (maskingKey.length == 4) {
            masked = true;
        } else {
            throw new BadValueException("the masking key MUST be 4 bytes.");
        }
        
        /* get the header */
        out.write(factoredEncode());
        out.write(maskingKey);
        out.write(util.Util.applyMaskingAlgorithm(maskingKey, getPayload()));
        out.flush();
        
    }// end of mask encode
    
    /**
     * Serializes this web socket frame to a byte array WITHOUT masking.
     * 
     * @return the serialized frame
     * @throws IOException
     */
    public byte[] encode() throws IOException
    {
    
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        OutputStream out = new BufferedOutputStream(buffer);
        this.encode(out);
        buffer.flush();
        byte[] encodedByteFrame = buffer.toByteArray();
        buffer.close();
        return encodedByteFrame;
        
    }
    
    /**
     * Serializes this web socket frame to a byte array WITH masking.
     * 
     * @param maskingKey
     * the 4 byte masking key to use for encoding
     * @return the serialized frame
     * @throws BadValueException
     * if the masking key given is invalid (not 4 bytes) or is null
     * @throws IOException
     */
    public byte[] encode(byte[] maskingKey) throws BadValueException,
            IOException
    {
    
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        OutputStream out = new BufferedOutputStream(buffer);
        this.encode(out, maskingKey);
        buffer.flush();
        byte[] encodedByteFrame = buffer.toByteArray();
        buffer.close();
        return encodedByteFrame;
    }
    
    /**
     * Indicates the state of the opcode of this frame.
     * 
     * @return the opcode of this frame
     */
    public Opcode getOpcode() {
    
        return opcode;
    }
    
    /**
     * Gets a COPY of the payload stored in this frame.
     * 
     * @return a copy of the payload stored in this frame
     */
    public byte[] getPayload() {
    
        byte[] copyOfPayload = payload;
        return copyOfPayload;
    }
    
    /**
     * Indicates the state of the fin bit of this frame.
     * 
     * @return true if the fin bit is set, false otherwise
     */
    public boolean isFin() {
    
        return fin;
    }
    
    /**
     * Factored out code for encoding a web socket frame object's header into a
     * byte array.
     * 
     * @return
     * @throws BadValueException
     */
    private byte[] factoredEncode() {
    
        /* the first two required bytes for the frame header */
        byte[] frameHeader = new byte[2];
        frameHeader[0] = 0x0;
        if (isFin()) {
            frameHeader[0] = (byte) (frameHeader[0] | 0x80);
        }
        frameHeader[0] = (byte) (frameHeader[0] | getOpcode().label);
        frameHeader[1] = 0x0;
        if (masked) {
            frameHeader[1] = (byte) (frameHeader[1] | 0x80);
        }
        byte[] extPayloadLeng = null;
        
        long payloadLength = getPayload().length;
        // check if payload length is 125 or less
        if (payloadLength < 126) {
            frameHeader[1] = (byte) (frameHeader[1] | (byte) payloadLength);
        }
        /* maximum number that can fit two bytes */
        if (payloadLength <= 65535 && payloadLength > 125) {
            frameHeader[1] = (byte) (frameHeader[1] | 0x7E);
            extPayloadLeng = new byte[2];
            util.Util.encodeLongToBytes(extPayloadLeng, getPayload().length, 0,
                    Short.SIZE / Byte.SIZE);
            
        } else if (payloadLength > 65535) {
            frameHeader[1] = (byte) (frameHeader[1] | 0x7F);
            extPayloadLeng = new byte[8];
            util.Util.encodeLongToBytes(extPayloadLeng, getPayload().length, 0,
                    Long.SIZE / Byte.SIZE);
            // set the most significant bit to 0 as required by RFC 6455
            extPayloadLeng[0] = (byte) (extPayloadLeng[0] & 0x7F);
        }
        
        /* Send the frame header followed by payload length if the payload
         * length more than 125 bytes long add extended payload length data to
         * the header */
        if (payloadLength > 125) {
            byte[] combinedHeader = new byte[frameHeader.length
                    + extPayloadLeng.length];
            System.arraycopy(frameHeader, 0, combinedHeader, 0,
                    frameHeader.length);
            System.arraycopy(extPayloadLeng, 0, combinedHeader,
                    frameHeader.length, extPayloadLeng.length);
            return combinedHeader;
        } else {
            return frameHeader;
        }
    }// end of factored encode
    
    /**
     * helper function to validate a frame
     * 
     * @throws BadValueException
     */
    private void validateFrame() throws BadValueException {
    
        /* test if the opcode is the control opcode and if it is cut the
         * payload lenght to be 125 bytes or less as specified in RFC 6455
         * 5.5 */
        if ((getOpcode().getValue() & 0x8) != 0) {
            
            /* 2. This frame is a control frame with a payload length
             * greater than 125 bytes */
            if (payload != null) {
                if (payload.length > 125) { throw new BadValueException(
                        "The control payload cannot be more than 125 bytes"); }
            } else {
                payload = new byte[0];
            }
            if (!isFin()) { throw new BadValueException(
                    "The control frame must be fin"); }
        }
    }
    
    /**
     * helper function to read several bytes from input stream
     * 
     * @param in
     * @param byteArr
     * @return
     * @throws IOException
     */
    private int fillByteArray(InputStream in, byte[] byteArr)
            throws IOException {
    
        int bytesRead = 0;
        int tempInt;
        // while ((tempInt = in.read()) != -1 && bytesRead < byteArr.length) {
        // byteArr[bytesRead] = (byte) tempInt;
        // bytesRead++;
        // }
        
        for (int i = 0; i < byteArr.length; i++) {
            if ((tempInt = in.read()) == -1) {
                if (bytesRead > 0) {
                    return bytesRead;
                } else {
                    return -1;
                }
            }
            
            byteArr[i] = (byte) tempInt;
            bytesRead++;
        }
        
        if (bytesRead == 0) {
            return -1;
        } else {
            return bytesRead;
        }
        
    }
}

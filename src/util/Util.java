package util;

// this import is used for long to byte conversion
import exception.BadValueException;

/**
 * @author Rovshen Nazarov. This class contains helper functions
 */
public class Util {
    
    /**
     * This function gets a long value and produces byte array
     * The function was inspired by the book Kaufmann and Donahoo TCPIP Sockets
     * in Java Practical Guide for Programmers 2nd Edition.Feb
     * 
     * @param inputLong
     * : long
     * @return number of bytes read
     * @throws BadValueException
     */
    public static int encodeLongToBytes(byte[] result, final long inputLong,
            int offset, int size)
    {
    
        for (int i = 0; i < size; i++) {
            result[offset++] = (byte) (inputLong >> ((size - i - 1) * Byte.SIZE));
        }
        return offset;
        
    }
    
    /**
     * This function gets a byte array value and generates long value
     * The function was inspired by the book Kaufmann and Donahoo TCPIP Sockets
     * in Java Practical Guide for Programmers 2nd Edition.Feb
     * 
     * @param inputByteArr
     * : byte[]
     * @return long value
     */
    
    public static long decodeByteArrToLong(final byte[] inputByteArr,
            int stepSize, int resultSize)
    {
    
        long resultLong = 0;
        final int MASK = 0xFF; // octave
        
        for (int i = 0; i < resultSize; i++) {
            resultLong = (resultLong << Byte.SIZE)
                    | ((long) inputByteArr[stepSize + i] & MASK);
        }
        return resultLong;
    }
    
    /**
     * This function takes as an input a masking key and a byte array to be
     * masked. This function implements an algorithm for masking using XOR
     * operator on bytes and described in RFC 6455
     * 
     * @param maskingKey
     * byte array
     * @param payload
     * byte array
     * @return masked/unmasked byte array
     * @throws BadValueException
     */
    public static byte[] applyMaskingAlgorithm(byte[] maskingKey, byte[] payload) {
    
        byte[] result = new byte[payload.length];
        // ^ xor operator
        // octet is a set of 8 bits
        
        for (int i = 0; i < payload.length; i++) {
            result[i] = (byte) (payload[i] ^ maskingKey[i % 4]);
        }
        
        return result;
    }
    
}

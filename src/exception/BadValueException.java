package exception;

/**
 * @author Rovshen Nazarov Bad Value Exception is used to throw exceptions on an
 * incorrectly encoded frame. The message for exception is defined when
 * the exception is thrown
 */
public class BadValueException extends Exception {
    
    /**
     * Serialization ID for the exception
     */
    private static final long serialVersionUID = -6790478034540404953L;
    
    /**
     * Constructs an exception with a description of the problem
     * 
     * @param message
     * the description of the problem
     */
    public BadValueException(String message) {
    
        super(message);
    }
    
    /**
     * Constructs an exception with a description of the problem and
     * an other exception that originally caused this problem.
     * 
     * @param message
     * the description of the problem
     * @param cause
     * the exception which originally encountered this problem
     */
    public BadValueException(String message, Throwable cause)
    {
    
        super(message, cause);
    }
    
    /**
     * Auto-generated serialization key.
     */
}

package test;

/*A suite of tests for the web socket frame*/
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ WebSocketFrameDecodeTest.class, WebSocketFrameEncodeTest.class,
})
public class WebSocketFrameAllTests {
    
}

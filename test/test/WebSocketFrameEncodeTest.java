package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import model.WebSocketFrame;
import model.WebSocketFrame.Opcode;

import org.junit.Assert;
import org.junit.Test;

import exception.BadValueException;

/**
 * Tests the encode function of the WebSocketFrame
 * 
 * @author Left Team: Ryan Henning, Roman Smetana, Rovshen Nazarov
 */

public class WebSocketFrameEncodeTest {
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the FIN bit in the byte array.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeFinTrue() throws IOException, BadValueException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 1 - fin bit true, expected result with the first byte's most
         * significant bit */
        Assert.assertEquals(1, (arr[0] & 0x80) >> 7);
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the FIN bit in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeFinFalse() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 0 - fin bit false, expected result with the first byte's most
         * significant bit */
        Assert.assertEquals(0, (arr[0] & 0x80) >> 7);
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the RSV1 bit in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeRSV1() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 0 - RSV1 bit false, expected result with the first byte's
         * content. */
        Assert.assertEquals(0, (arr[0] & 0x40) >> 6);
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the RSV2 bit in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeRSV2() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 0 - RSV2 bit false, expected result with the first byte's
         * content. */
        Assert.assertEquals(0, (arr[0] & 0x20) >> 5);
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the RSV3 bit in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeRSV3() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 0 - RSV3 bit false, expected result with the first byte's
         * content. */
        Assert.assertEquals(0, (arr[0] & 0x10) >> 4);
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the PING Opcode in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeOpcode() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 0x09 - which is an Opcode for PING (expected result) with the
         * first byte's content. */
        Assert.assertEquals(0x09, (arr[0] & 0xF));
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the Mask flag in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeMaskFlagFalse() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 0 - which is false for the masking key (expected result) with
         * the second byte's content. */
        Assert.assertEquals(0, (arr[1] & 0x80) >> 7);
    }
    
    /**
     * Use encoding with masking to encode a web socket frame object into the
     * byte array and then test the Mask flag in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeMaskFlagTrue() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame using masking */
        frame.encode(new DataOutputStream(buf), getExampleMaskingKey());
        byte[] arr = buf.toByteArray();
        
        /* compare 1 - which is true for the masking key (expected result) with
         * the second byte's content. */
        Assert.assertEquals(1, (arr[1] & 0x80) >> 7);
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload length bytes in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeEmptyPayloadLenght() throws BadValueException,
            IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare 0 - which is a payload of length 0 (expected result) with the
         * second byte's content. */
        Assert.assertEquals(0, (arr[1] & 0x7F));
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload length bytes in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test
    public void encodeShortPayloadLenght() throws BadValueException,
            IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.BINARY_FRAME,
                getExamplePayload());
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare predefined payload's length - (expected result) with the
         * second byte's content. */
        Assert.assertEquals(getExamplePayload().length, (arr[1] & 0x7F));
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload length bytes in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test
    public void encodeMediumPayloadLenght() throws BadValueException,
            IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* medium payload that will occupy 2 bytes */
        byte[] testArr = new byte[300];
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.BINARY_FRAME,
                testArr);
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare payload length flag of 126 value, which signals that next two
         * bytes are the content length. */
        Assert.assertEquals(126, (arr[1] & 0x7F));
        /* compare predefined payload's length - (expected result) with the 3rd
         * and 4th bytes' content. */
        Assert.assertEquals(testArr.length,
                ByteBuffer.wrap(new byte[] { arr[2], arr[3] }).getShort());
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload length bytes in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test
    public void encodeLongPayloadLenght() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* big payload that will occupy 8 bytes */
        byte[] testArr = new byte[3 * Short.MAX_VALUE];
        /* generate a web socket frame object with a long payload data */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.BINARY_FRAME,
                testArr);
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare payload length flag of 127 value, which signals that next 8
         * bytes are the content length. */
        Assert.assertEquals(127, (arr[1] & 0x7F));
        /* compare predefined payload's length - (expected result) with the 3rd
         * to 10th bytes' content. */
        Assert.assertEquals(
                testArr.length,
                ByteBuffer.wrap(
                        new byte[] { arr[2], arr[3], arr[4], arr[5], arr[6],
                                arr[7], arr[8], arr[9] }).getLong());
    }
    
    /**
     * Use encoding with masking to encode a web socket frame object into the
     * byte array and then test the masking key bytes in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeMaskingKey() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame with masking */
        frame.encode(new DataOutputStream(buf), getExampleMaskingKey());
        byte[] arr = buf.toByteArray();
        
        /* compare predefined masking key (expected result) with the 3rd to 6th
         * bytes' content. */
        Assert.assertArrayEquals(getExampleMaskingKey(), new byte[] { arr[2],
                arr[3], arr[4], arr[5] });
    }
    
    /**
     * Use encoding with masking to encode a web socket frame object. The
     * masking key is invalid, so the BadValueException is expected.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeInvalidMaskingKey() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame with an invalid masking key,
         * BadValueException is expected */
        frame.encode(new DataOutputStream(buf), new byte[] { 1, 2 });
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload length bytes in the byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeEmptyPayload() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                new byte[0]);
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare predefined the web socket length - expected 2 bytes with no
         * more data. */
        Assert.assertEquals(2, arr.length);
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload bytes in the result byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeShortPayload() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.PING,
                getExamplePayload());
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare predefined payload byte array- (expected result) with the
         * second payload byte array from the output stream. */
        Assert.assertArrayEquals(getExamplePayload(),
                Arrays.copyOfRange(arr, 2, arr.length));
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload bytes in the result byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test
    public void encodeMediumPayload() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        byte[] testArr = new byte[300];
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.BINARY_FRAME,
                testArr);
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare predefined payload byte array- (expected result) with the
         * second payload byte array from the output stream. */
        Assert.assertArrayEquals(testArr,
                Arrays.copyOfRange(arr, 4, arr.length));
    }
    
    /**
     * Use encoding without masking to encode a web socket frame object into the
     * byte array and then test the payload bytes in the result byte array.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test
    public void encodeLongPayload() throws BadValueException, IOException {
    
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        byte[] testArr = new byte[3 * Short.MAX_VALUE];
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false, Opcode.BINARY_FRAME,
                testArr);
        /* encode the web socket frame with no masking */
        frame.encode(new DataOutputStream(buf));
        byte[] arr = buf.toByteArray();
        
        /* compare predefined payload byte array- (expected result) with the
         * second payload byte array from the output stream. */
        Assert.assertArrayEquals(testArr,
                Arrays.copyOfRange(arr, 10, arr.length));
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object. This test is from the examples given by RFC 6455,
     * Section 5.7.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExample1() throws IOException, BadValueException {
    
        /* byte 1 - the fin set, the opcode 1, byte 2 - payload length 5, bytes
         * 3 - 7 the "Hello" world */
        byte[] buffer = { (byte) 0x81, 0x05, 0x48, 0x65, 0x6c, 0x6c, 0x6f };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.TEXT_FRAME, helperGetTextPayload("Hello"));
        byte[] mask = null;
        
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object. This test is from the examples given by RFC 6455,
     * Section 5.7.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExample2() throws IOException, BadValueException {
    
        /* byte 1 - the fin set, the opcode 1, byte 2 - 3 - payload length 5 and
         * the mask bit set, bytes 4 - 7 masking key, bytes 8 - 11 the "Hello"
         * world */
        byte[] buffer = { (byte) 0x81, (byte) 0x85, 0x37, (byte) 0xfa, 0x21,
                0x3d, 0x7f, (byte) 0x9f, 0x4d, 0x51, 0x58 };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.TEXT_FRAME, helperGetTextPayload("Hello"));
        byte[] mask = { 0x37, (byte) 0xfa, 0x21, 0x3d };
        
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object. This test is from the examples given by RFC 6455,
     * Section 5.7.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExample3_1() throws IOException, BadValueException {
    
        /* byte 1 - the opcode 1, byte 2 - payload length 3, bytes 3 - 5 the
         * "Hel" world */
        byte[] buffer = { 0x01, 0x03, 0x48, 0x65, 0x6c };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false,
                WebSocketFrame.Opcode.TEXT_FRAME, helperGetTextPayload("Hel"));
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object. This test is from the examples given by RFC 6455,
     * Section 5.7.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExample3_2() throws IOException, BadValueException {
    
        /* byte 1 - the opcode 0 and the fin bit set, byte 2 - payload length 2,
         * bytes 3 - 5 the "lo" world */
        byte[] buffer = { (byte) 0x80, 0x02, 0x6c, 0x6f };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.CONTINUATION_FRAME,
                helperGetTextPayload("lo"));
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object. This test is from the examples given by RFC 6455,
     * Section 5.7.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExample4_1() throws IOException, BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is Ping, byte 2 - payload
         * length 6, bytes 3 - 8 the "World!" world */
        byte[] buffer = { (byte) 0x89, 0x06, 87, 111, 114, 108, 100, 33 };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.PING, helperGetTextPayload("World!"));
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object. This test is from the examples given by RFC 6455,
     * Section 5.7.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExample4_2() throws IOException, BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is Pong, byte 2 - mask bit is
         * set and the payload length is 5, byte 4 - 7 are masking bytes, bytes
         * 8 - 11 the "Hello" world */
        byte[] buffer = { (byte) 0x8a, (byte) 0x85, 0x37, (byte) 0xfa, 0x21,
                0x3d, 0x7f, (byte) 0x9f, 0x4d, 0x51, 0x58 };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.PONG, helperGetTextPayload("Hello"));
        byte[] mask = { 0x37, (byte) 0xfa, 0x21, 0x3d };
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExampleMediumPayload() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, byte 2 - the payload
         * length is 126, bytes - 3 - 4 is the real payload length 256, bytes 5
         * + 256 the medium payload */
        byte[] header = { (byte) 0x82, 0x7E, 0x01, 0x00 };
        byte[] payload = new byte[256];
        for (int i = 0; i < 256; i++) {
            payload[i] = (byte) (Math.random() * 256);
        }
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.BINARY_FRAME, payload);
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array constructed from the header and the payload. The two arrays
         * must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask),
                helperConcatArrays(header, payload));
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object. The long payload is randomly generated.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeExampleLongPayload() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, byte 2 - the payload
         * length is 127, bytes - 3 - 10 is the real payload length 65536,
         * bytes
         * 11 + 65536 the long payload */
        byte[] header = { (byte) 0x82, 0x7F, 0, 0, 0, 0, 0, 1, 0, 0 };
        byte[] payload = new byte[65536];
        for (int i = 0; i < 65536; i++) {
            payload[i] = (byte) (Math.random() * 256);
        }
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.BINARY_FRAME, payload);
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array constructed from the header and the payload. The two arrays
         * must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask),
                helperConcatArrays(header, payload));
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeClose() throws IOException, BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is set to Close Connection,
         * byte 2 - the payload length is 26, bytes 3 + 25
         * "I want to close!!! Thanks." */
        byte[] buffer = { (byte) 0x81, 26, 73, 32, 119, 97, 110, 116, 32, 116,
                111, 32, 99, 108, 111, 115, 101, 33, 33, 33, 32, 84, 104, 97,
                110, 107, 115, 46 };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.TEXT_FRAME,
                helperGetTextPayload("I want to close!!! Thanks."));
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeNotFIN() throws IOException, BadValueException {
    
        /* byte 1 - the fin bit is NOT set and the opcode is set to Close
         * Connection, byte 2 - the payload length is 26, bytes 3 + 25
         * "I want to close!!! Thanks." */
        byte[] buffer = { (byte) 0x01, 26, 73, 32, 119, 97, 110, 116, 32, 116,
                111, 32, 99, 108, 111, 115, 101, 33, 33, 33, 32, 84, 104, 97,
                110, 107, 115, 46 };
        /* generate a web socket frame object */
        WebSocketFrame frame = new WebSocketFrame(false,
                WebSocketFrame.Opcode.TEXT_FRAME,
                helperGetTextPayload("I want to close!!! Thanks."));
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Use web sockets object constructor to generate the web socket with null
     * opcode. A BadValueException is expected.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test(expected = BadValueException.class)
    public void encodeInvalidOpcode1() throws IOException, BadValueException {
    
        new WebSocketFrame(false, null, new byte[0]);
    }
    
    /**
     * Use web sockets object setOpcode setter to set the web socket with null
     * opcode. A BadValueException is expected.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test(expected = BadValueException.class)
    public void encodeInvalidOpcode2() throws IOException, BadValueException {
    
        new WebSocketFrame(false,
                null, new byte[0]);
        
    }
    
    /**
     * Use web sockets object constructor to generate the web socket with a
     * payload of length 0.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeEmptyLength() throws IOException, BadValueException {
    
        byte[] buffer = { (byte) 0x81, 0x00 };
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.TEXT_FRAME, helperGetTextPayload(""));
        byte[] mask = null;
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(buffer, helperGetEncodedFrame(frame, mask));
    }
    
    /**
     * Throw an IOException when 'out' is null. *
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test(expected = IOException.class)
    public void encodeNullOut() throws IOException, BadValueException {
    
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.TEXT_FRAME, helperGetTextPayload(""));
        frame.encode(null, null);
    }
    
    /**
     * Throw an IOException when 'out' is null with the mask. *
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test(expected = IOException.class)
    public void encodeNullOutMasked() throws IOException, BadValueException {
    
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.TEXT_FRAME, helperGetTextPayload(""));
        frame.encode(null, getExampleMaskingKey());
    }
    
    /**
     * Use web sockets object constructor to generate the web socket with a
     * payload of length 0 with the mask.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeEmptyLengthWithMask() throws IOException,
            BadValueException {
    
        byte[] buffer = { (byte) 0x81, (byte) 0x80, 0x01, 0x02, 0x03, 0x04 };
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.TEXT_FRAME, helperGetTextPayload(""));
        byte[] mask = { 0x01, 0x02, 0x03, 0x04 };
        /* Using the helper function for web socket frame encoding, get the byte
         * array of the encoded web socket and compare it to predefined byte
         * array. The two arrays must be equal. */
        assertArrayEquals(buffer, helperGetEncodedFrame(frame, mask));
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeDifferentLengths0_125() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, the following bytes are
         * the payload length 125, then the following bytes represent the
         * payload */
        for (byte len = 0; len <= 125; len++) {
            byte[] header = { (byte) 0x82, len };
            byte[] payload = new byte[len];
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.BINARY_FRAME, payload);
            byte[] mask = null;
            /* Using the helper function for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header and the payload. The two
             * arrays must be equal. */
            assertArrayEquals(helperGetEncodedFrame(frame, mask),
                    helperConcatArrays(header, payload));
        }
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeDifferentLengthsMasked0_125() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, the following bytes are
         * the payload length 125 and the mask bit set, then the 4 bytes that
         * represent the mask, then the following bytes represent the payload. */
        
        /* for each length test the encode of that length payload */
        for (byte len = 0; len <= 125; len++) {
            byte[] header = { (byte) 0x82, (byte) (0x80 | len) };
            byte[] mask = new byte[4];
            
            /* generate the mask randomly */
            for (int i = 0; i < 4; i++) {
                mask[i] = (byte) (Math.random() * 256);
            }
            byte[] payload = new byte[len];
            
            /* generate random payload using the currently processing lengths */
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.BINARY_FRAME, helperMaskPayload(
                            payload, mask));
            /* Using the helper functions for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header, the mask and the payload.
             * The two arrays must be equal. */
            assertArrayEquals(
                    helperGetEncodedFrame(frame, mask),
                    helperConcatArrays(header,
                            helperConcatArrays(mask, payload)));
        }
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeDifferentLengths126_65535() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, the following bytes are
         * the payload length, then the following bytes represent the payload. */
        
        /* for each length test the encode of that length payload */
        for (int len = 126; len <= 65000; len++) {
            byte[] header = { (byte) 0x82, 126, (byte) (len / 256),
                    (byte) (len % 256) };
            byte[] payload = new byte[len];
            
            /* generate random payload using the currently processing lengths */
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.BINARY_FRAME, payload);
            byte[] mask = null;
            /* Using the helper function for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header and the payload. The two
             * arrays must be equal. */
            assertArrayEquals(helperConcatArrays(header, payload),
                    helperGetEncodedFrame(frame, mask));
            /* if we reached 1000, we can skip forward to decrease test's
             * running time */
            if (len == 1000) {
                len = 65000;
            }
        }
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeDifferentLengthsMasked126_65535() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, the following bytes are
         * the payload length and the mask bit set, then the 4 bytes that
         * represent the mask, then the following bytes represent the payload. */
        
        /* for each length test the encode of that length payload */
        for (int len = 126; len <= 64535; len++) {
            
            /* the 256 is used to split length into two bytes */
            byte[] header = { (byte) 0x82, (byte) (0x80 | 126),
                    (byte) (len / 256), (byte) (len % 256) };
            byte[] mask = new byte[4];
            
            /* generate the mask randomly */
            for (int i = 0; i < 4; i++) {
                mask[i] = (byte) (Math.random() * 256);
            }
            byte[] payload = new byte[len];
            
            /* generate random payload using the currently processing lengths */
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.BINARY_FRAME, helperMaskPayload(
                            payload, mask));
            /* Using the helper functions for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header, the mask and the payload.
             * The two arrays must be equal. */
            assertArrayEquals(
                    helperGetEncodedFrame(frame, mask),
                    helperConcatArrays(header,
                            helperConcatArrays(mask, payload)));
            
            /* if we reached 1000, we can skip forward to decrease test's
             * running time */
            if (len == 1000) {
                len = 65000;
            }
        }
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeDifferentLengths65536_65799() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, the following bytes are
         * the payload length, then the following bytes represent the payload. */
        
        /* for each length test the encode of that length payload */
        for (int len = 65536; len <= 65799; len++) {
            byte[] header = { (byte) 0x82, 127, 0, 0, 0, 0, 0,
                    (byte) (len / 65535), (byte) (len / 256),
                    (byte) (len % 256) };
            byte[] payload = new byte[len];
            
            /* generate random payload using the currently processing lengths */
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.BINARY_FRAME, payload);
            byte[] mask = null;
            /* Using the helper function for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header and the payload. The two
             * arrays must be equal. */
            assertArrayEquals(helperGetEncodedFrame(frame, mask),
                    helperConcatArrays(header, payload));
        }
    }
    
    /**
     * Using a byte array with bytes related to a specific web socket frame,
     * compare the bytes array with encoded byte array by the encode method of
     * the web socket object.
     * 
     * @throws IOException
     * @throws BadValueException
     */
    @Test
    public void encodeDifferentLengthsMasked65536_65799() throws IOException,
            BadValueException {
    
        /* byte 1 - the fin bit set and the opcode is 2, the following bytes are
         * the payload length and the mask bit set, then the 4 bytes that
         * represent the mask, then the following bytes represent the payload. */
        
        /* for each length test the encode of that length payload */
        for (int len = 65536; len <= 65799; len++) {
            
            /* the 65535 and 256 above are used to split length into bytes */
            byte[] header = { (byte) 0x82, (byte) (0x80 | 127), 0, 0, 0, 0, 0,
                    (byte) (len / 65535), (byte) (len / 256),
                    (byte) (len % 256) };
            byte[] mask = new byte[4];
            
            /* generate the mask randomly */
            for (int i = 0; i < 4; i++) {
                mask[i] = (byte) (Math.random() * 256);
            }
            byte[] payload = new byte[len];
            
            /* generate random payload using the currently processing lengths */
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.BINARY_FRAME, helperMaskPayload(
                            payload, mask));
            
            /* Using the helper functions for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header, the mask and the payload.
             * The two arrays must be equal. */
            assertArrayEquals(
                    helperGetEncodedFrame(frame, mask),
                    helperConcatArrays(header,
                            helperConcatArrays(mask, payload)));
        }
    }
    
    /**
     * Ensures that you can encode a close frame with an empty payload.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test
    public void encodeTestCloseWithoutPayload() throws BadValueException,
            IOException {
    
        byte[] buffer = { (byte) 0x88, 0x00 };
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.CLOSE_CONNECTION, new byte[0]);
        byte[] mask = null;
        assertArrayEquals(helperGetEncodedFrame(frame, mask), buffer);
    }
    
    /**
     * Ensures that you cannot encode a close frame with a non-zero-length
     * payload.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test
    public void encodeTestCloseWithPayload() throws BadValueException,
            IOException {
    
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.CLOSE_CONNECTION,
                helperGetTextPayload("Hello"));
        helperGetEncodedFrame(frame, null);
    }
    
    /**
     * Ensures that a ping frame cannot be encoded when the payload length is
     * 126 bytes, but may be encoded with less bytes.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeTestPingPayloadLengths() throws BadValueException,
            IOException {
    
        // Encode ping messages with payloads up to 125 bytes. This should all
        // work.
        for (byte len = 0; len <= 125; len++) {
            byte[] header = { (byte) 0x89, len };
            byte[] payload = new byte[len];
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.PING, payload);
            byte[] mask = null;
            /* Using the helper function for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header and the payload. The two
             * arrays must be equal. */
            assertArrayEquals(helperGetEncodedFrame(frame, mask),
                    helperConcatArrays(header, payload));
        }
        
        // Encode a ping message with a payload of 126 bytes. This should fail.
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.PING, new byte[126]);
        helperGetEncodedFrame(frame, null);
    }
    
    /**
     * Ensures that a poing frame cannot be encoded when the payload length is
     * 126 bytes, but may be encoded with less bytes.
     * 
     * @throws BadValueException
     * @throws IOException
     */
    @Test(expected = BadValueException.class)
    public void encodeTestPongPayloadLengths() throws BadValueException,
            IOException {
    
        // Encode pong messages with payloads up to 125 bytes. This should all
        // work.
        for (byte len = 1; len <= 125; len++) {
            byte[] header = { (byte) 0x8A, len };
            byte[] payload = new byte[len];
            for (int i = 0; i < len; i++) {
                payload[i] = (byte) (Math.random() * 256);
            }
            WebSocketFrame frame = new WebSocketFrame(true,
                    WebSocketFrame.Opcode.PONG, payload);
            byte[] mask = null;
            /* Using the helper function for web socket frame encoding, get the
             * byte array of the encoded web socket and compare it to
             * predefined
             * byte array constructed from the header and the payload. The two
             * arrays must be equal. */
            assertArrayEquals(helperConcatArrays(header, payload),
                    helperGetEncodedFrame(frame, mask));
        }
        
        // Encode a pong message with a payload of 126 bytes. This should fail.
        WebSocketFrame frame = new WebSocketFrame(true,
                WebSocketFrame.Opcode.PONG, new byte[126]);
        helperGetEncodedFrame(frame, null);
    }
    
    /**
     * This helper function gets as input a web socket frame and a mask (can be
     * null). Using input data the encoding is performed with or without masking
     * key. The result of the encoding is stored into the local byte array
     * output stream from which a byte array is generated and returned to the
     * caller.s
     * 
     * @param frame
     * @param mask
     * @return byte array of the encouded web socket frame
     * @throws IOException
     * @throws BadValueException
     */
    private static byte[] helperGetEncodedFrame(WebSocketFrame frame,
            byte[] mask) throws IOException, BadValueException {
    
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        if (mask != null) {
            frame.encode(out, mask);
        } else {
            frame.encode(out);
        }
        return out.toByteArray();
    }
    
    /**
     * This helper function takes as input a string and converts it using
     * specified by RFC 6455 string format and returns byte array to the caller.
     * 
     * @param str
     * @return byte array
     */
    public static byte[] helperGetTextPayload(String str) {
    
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            fail("?");
        }
        return null;
    }
    
    /**
     * This helper function gets as an input two arrays and copies them
     * completely into a new array, doing so creates new concatenated array.
     * 
     * @param sourceArr
     * @param destinationArr
     * @return the combined array
     */
    private static byte[] helperConcatArrays(byte[] sourceArr,
            byte[] destinationArr) {
    
        byte[] combinedArr = new byte[sourceArr.length + destinationArr.length];
        System.arraycopy(sourceArr, 0, combinedArr, 0, sourceArr.length);
        System.arraycopy(destinationArr, 0, combinedArr, sourceArr.length,
                destinationArr.length);
        return combinedArr;
    }
    
    /**
     * This helper function gets as the input payload and applies to the payload
     * an algorithm to mask it with the supplied mask. The algorithm for masking
     * payload byte array is specified in RFC 6455.
     * 
     * @param payload
     * @param mask
     * @return
     */
    private static byte[] helperMaskPayload(byte[] payload, byte[] mask) {
    
        byte[] copy = Arrays.copyOf(payload, payload.length);
        for (int i = 0; i < payload.length; i++) {
            /* XOR each byte of the payload with a byte from the bask selected
             * using module of 4 */
            copy[i] ^= mask[i % 4];
        }
        return copy;
    }
    
    /**
     * This helper function returns predefined byte array of a sample payload
     * data.
     * 
     * @return byte array
     */
    private byte[] getExamplePayload() {
    
        byte[] arr = new byte[] { -87, -29, -112, -67, -92, 51, 76, 37, 114,
                -98, 108, 62, -31, 75, 80, -27, -65, 47, 8, 60, -70, 4, -62,
                44, -63, 22, 42, 105, 118, 120, 14, 67, -82, 57, 66, -57, -28,
                88, -79, -15, 68, 106 };
        
        return arr;
    }
    
    /**
     * This helper function returns predefined byte array of a sample masking
     * key.
     * 
     * @return byte array
     */
    private byte[] getExampleMaskingKey() {
    
        return new byte[] { 123, 62, -21, 5 };
    }
    
}
